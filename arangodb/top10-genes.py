from arango.core import Connection
import time

start = time.time()

db = Connection(db="_system")
toDict = lambda c, i: i

query01 = """

FOR g IN genes
    LET variants = (
        FOR m IN mutations
        FILTER m.chromosome == g.CHROMOSOME && m.start >= g.START && m.start <= g.END
        RETURN m
    )
SORT LENGTH(variants) DESC
LIMIT 10
RETURN { "gene": g.SYMBOL, "total": LENGTH(variants) }

"""

for g in db.query(query01, wrapper=toDict):
    print(g)

diff = time.time() - start
print("Elapsed time: ", diff, " (avg. ", diff/1000, ")")


