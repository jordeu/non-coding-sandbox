#!/bin/bash

arangoimp --collection=mutations --file=mutations.10k.tsv --type=tsv --create-collection true
arangoimp --collection=genes --file=genes.1k.tsv --type=tsv --create-collection true