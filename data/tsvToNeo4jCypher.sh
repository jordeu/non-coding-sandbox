#!/bin/bash

cat genes.1k.tsv | tail -n +2 | awk -F'\t' '{ print("CREATE (n:GENE { symbol: \""$8"\", chromosome : \""$3"\", start : "$4", end : "$5"});") }' > genes.1k.cql
cat genes.10k.tsv | tail -n +2 | awk -F'\t' '{ print("CREATE (n:GENE { symbol: \""$8"\", chromosome : \""$3"\", start : "$4", end : "$5"});") }' > genes.10k.cql
cat genes.62k.tsv | tail -n +2 | awk -F'\t' '{ print("CREATE (n:GENE { symbol: \""$8"\", chromosome : \""$3"\", start : "$4", end : "$5"});") }' > genes.62k.cql
cat mutations.10k.tsv | tail -n +2 | awk -F'\t' '{ print("CREATE (n:VARIANT { chromosome: \""$1"\", start: "$2", end: "$3", change: \""$4"\"});") }' > mutations.10k.cql
cat mutations.100k.tsv | tail -n +2 | awk -F'\t' '{ print("CREATE (n:VARIANT { chromosome: \""$1"\", start: "$2", end: "$3", change: \""$4"\"});") }' > mutations.100k.cql
cat mutations.344k.tsv | tail -n +2 | awk -F'\t' '{ print("CREATE (n:VARIANT { chromosome: \""$1"\", start: "$2", end: "$3", change: \""$4"\"});") }' > mutations.344k.cql

echo -e "l:label\tsymbol\tchromosome\tstart:int\tend:int" > genes.1k.n4j
cat genes.1k.n4j > genes.10k.n4j
cat genes.10k.n4j > genes.62k.n4j
cat genes.1k.tsv | tail -n +2 | awk -F'\t' '{ print("GENE\t"$8"\t"$3"\t"$4"\t"$5) }' >> genes.1k.n4j
cat genes.10k.tsv | tail -n +2 | awk -F'\t' '{ print("GENE\t"$8"\t"$3"\t"$4"\t"$5) }' >> genes.10k.n4j
cat genes.62k.tsv | tail -n +2 | awk -F'\t' '{ print("GENE\t"$8"\t"$3"\t"$4"\t"$5) }' >> genes.62k.n4j

echo -e "l:label\tchromosome\tstart:int\tend:int\tchange" > mutations.10k.n4j
cat mutations.10k.n4j > mutations.100k.n4j
cat mutations.10k.n4j > mutations.344k.n4j
cat mutations.10k.tsv | tail -n +2 | awk -F'\t' '{ print("VARIANT\t"$1"\t"$2"\t"$3"\t"$4) }' >> mutations.10k.n4j
cat mutations.100k.tsv | tail -n +2 | awk -F'\t' '{ print("VARIANT\t"$1"\t"$2"\t"$3"\t"$4) }' >> mutations.100k.n4j
cat mutations.344k.tsv | tail -n +2 | awk -F'\t' '{ print("VARIANT\t"$1"\t"$2"\t"$3"\t"$4) }' >> mutations.344k.n4j
