from elasticsearch import Elasticsearch
import time
import json

client = Elasticsearch([{'host': 'localhost'}])

start = time.time()

# Query top 10
result = client.search(index="genes", body=
    {
         "from" : 0,
         "size" : 0,
         "sort" : [],
         "aggregations": {
             "mutations" : {
                 "nested" : { "path" : "variants" },
                 "aggregations" : {
                     "mut-count" : { "value-count": { "field" : "variants.start" }}
                 }
             }
         }
    })

print(json.dumps(result, indent=4))

diff = time.time() - start
print("Elapsed time: ", diff, " (avg. ", diff/62000, ")")




