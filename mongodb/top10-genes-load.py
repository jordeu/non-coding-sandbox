from pymongo import MongoClient
import pandas as pd
import numpy as np
import time
import json

client = MongoClient()
db = client.test

genes = pd.read_csv('../data/genes.1k.tsv', sep='\t', header=0)
mutations = pd.read_csv('../data/mutations.10k.tsv', sep='\t', header=0, dtype={'chromosome': np.object})

start = time.time()

# Join and load the data
for count, gene in genes.iterrows():
    variants = mutations[
        (mutations.chromosome == gene.CHROMOSOME) &
        (mutations.start >= gene.START) &
        (mutations.start <= gene.END)
    ]

    if len(variants) == 0:
        continue

    geneJson = json.loads(gene.T.to_json())
    geneJson['variants'] = json.loads(variants.to_json(orient="records"))

    db.genes.insert(geneJson)

diff = time.time() - start
print("Loading time: ", diff)




