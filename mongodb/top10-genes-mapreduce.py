from bson import SON
from pymongo import MongoClient
from bson.code import Code
import time
import json

client = MongoClient()
db = client.test

start = time.time()

map = Code("function() {"
           "   emit(this.SYMBOL, this.variants.length);"
           "}")

reduce = Code("function(key, values) { "
              "   return Array.sum(values);"
              "}")

result = db.genes.map_reduce(map, reduce, out=SON([("replace", "genes_variants_length"), ("db", "test")]), full_response=True)
result = db.genes_variants_length.find().sort("value", -1).limit(10)

for item in result:
    print(json.dumps(item, indent=4))

diff = time.time() - start
print("Elapsed time: ", diff, " (avg. ", diff/62000, ")")




