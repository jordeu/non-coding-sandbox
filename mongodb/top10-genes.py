from pymongo import MongoClient
import time
import json

client = MongoClient()
db = client.test

start = time.time()

# Query top 10
result = db.genes.aggregate([
    {"$project": {"symbol": "$SYMBOL", "variants": "$variants"}},
    {"$unwind": "$variants"},
    {"$group": {"_id": "$symbol", "count": {"$sum": 1}}},
    {"$sort": {"count": -1}},
    {"$limit": 10}
])

print(json.dumps(result["result"], indent=4))

diff = time.time() - start
print("Elapsed time: ", diff, " (avg. ", diff/62000, ")")




