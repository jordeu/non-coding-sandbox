from multiprocessing import Pool
from orientdb.client import *
import csv
import time

# Modules
insertMutations = False
insertGenes = False
insertEdges = True

# Config
maxQueue = 10
batchSize = 5000

# Database connection
try:
    user = 'root'
    password = 'cocorico'
    url = 'http://localhost:2480'
    server = Server(url=url, username=user, password=password)
except CompassException, e:
    print e
    exit(1)

db = server.database(name='noncoding', credentials=ADMIN)


def batch(data, count):
    db.batch(operations=data)
    print "{0} [DONE]".format(count)


# Insert all the variants
if insertMutations:

    pool = Pool()
    variantsFile = open('../data/mutations.10k.tsv', 'rb')
    mutations = db.klass(name='Mutations', create=False)

    variantsReader = csv.reader(variantsFile, delimiter='\t')
    headers = variantsReader.next()
    print("Inserting variants...")
    size = 0
    totalBatches = 0
    operations = []

    for row in variantsReader:

        operations.append({'type': 'c', 'record': {
            "@class": 'Mutations',
            'chromosome': row[0],
            'position': row[1],
            'change': row[3]
        }})

        size += 1

        if size == batchSize:
            totalBatches += 1
            count = totalBatches * batchSize

            # Wait pending
            while pool._taskqueue._qsize() > maxQueue:
                time.sleep(1)

            print "{0} [SEND]".format(count)

            # Submit the job
            pool.apply_async(batch, args=(operations, count))
            # db.batch(operations=operations)
            size = 0
            operations = []

    count = (totalBatches * batchSize) + size
    print "{0} [SEND]".format(count)
    pool.apply_async(batch, args=(operations, count))

    variantsFile.close()
    pool.close()
    pool.join()

if insertGenes:

    pool = Pool()
    genesFile = open('../data/genes.1k.tsv', 'rb')
    genes = db.klass(name='Genes', create=False)

    genesReader = csv.reader(genesFile, delimiter='\t')
    headers = genesReader.next()
    print("Inserting genes...")
    size = 0
    totalBatches = 0
    operations = []

    for row in genesReader:

        operations.append({'type': 'c', 'record': {
            "@class": 'Genes',
            'chromosome': row[2],
            'start': row[3],
            'end': row[4],
            'symbol': row[7]
        }})

        size += 1

        if size == batchSize:
            totalBatches += 1
            count = totalBatches * batchSize

            # Wait pending
            while pool._taskqueue._qsize() > maxQueue:
                time.sleep(1)

            print "{0} [SEND]".format(count)

            # Submit the job
            pool.apply_async(batch, args=(operations, count))
            # db.batch(operations=operations)
            size = 0
            operations = []

    count = (totalBatches * batchSize) + size
    print "{0} [SEND]".format(count)
    pool.apply_async(batch, args=(operations, count))

    genesFile.close()
    pool.close()
    pool.join()

if insertEdges:
    genes = db.query("SELECT * FROM Genes WHERE symbol='H6PD'")

    for key in genes.data:
        values = genes.data[key].data
        query = "create edge Mutated from {0} to (select from Mutations where chromosome=\"{1}\" and position >= {2} and position <= {3})" \
            .format(key, values['chromosome'], values['start'], values['end'])
        print query
        db.execute(query)





