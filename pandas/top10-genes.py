import pandas as pd
import numpy as np
import time

genes = pd.read_csv('../data/genes.10k.tsv', sep='\t', header=0)
mutations = pd.read_csv('../data/mutations.344k.tsv', sep='\t', header=0, dtype={'chromosome': np.object})

start = time.time()

unsortedTotals = []
for count, gene in genes.iterrows():
    variants = mutations[
        (mutations.chromosome == gene.CHROMOSOME) &
        (mutations.start >= gene.START) &
        (mutations.start <= gene.END)
    ]

    unsortedTotals.append({ "gene": gene.SYMBOL, "total": len(variants) })

sortedTotals = pd.DataFrame.from_dict(unsortedTotals).sort(columns=["total"], ascending=[0])
print(sortedTotals[0:10])

diff = time.time() - start
print("Elapsed time: ", diff, " (avg. ", diff/10000, ")")






